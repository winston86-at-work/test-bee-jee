<h1 class="page-title">
	<?php echo $page_title; ?>
</h1>
<table id="tasks" class="display">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Title</th>
            <th>Content</th>
            <th>Last Editor</th>
            <th>Status</th>
            <th>Created</th>
            <th>Tools</th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach ($data as $key => $task) { ?>
    		<tr>
	            <td><?php echo $task['id'] ?></td>
	            <td><?php echo $task['name'] ?></td>
	            <td><?php echo $task['email'] ?></td>
	            <td><?php echo $task['title'] ?></td>
	            <td><?php echo $task['content'] ?></td>
	            <td>
	            	<?php if (!empty($task['last_editor'])) { ?>
	            		<div class="alert alert-info">
		            		<?php echo $task['last_editor']; ?>
		            	</div>
	            	<?php }else{ ?>
						<div class="alert alert-info">
		            		Author
		            	</div>
	            	<?php } ?>
	            	
	            </td>
	            <td>
	            	<?php if(!empty($task['is_done'])){?>
						<div class="alert alert-success" role="alert">Done</div>
	            	<?php }else{ ?>
						<div class="alert alert-primary" role="alert">In Progress</div>
	            	<?php } ?>
	            </td>
	            <td><?php echo $task['created'] ?></td>
	            <td>
	            	<?php if (!empty($_SESSION['user'])) { ?>
		            	<a class="w-100 btn btn-success" href="/task/edit?id=<?php echo $task['id']; ?>">
		            		Edit
		            	</a> 
		            	<a class="w-100 btn btn-warning" href="/task/delete?id=<?php echo $task['id']; ?>">
		            		Delete
		            	</a> 
	            	<?php }else{ ?> 
						!
	            	<?php } ?>
	            </td>
	        </tr>
    	<?php } ?>
    </tbody>
</table>
<a href="/task/add" class="btn btn-primary">New Task</a>
<script type="text/javascript">
	$(document).ready(function(){
		$('#tasks').DataTable({
			lengthMenu: [[3, 10, 25, 50, -1], [3, 10, 25, 50, "All"]],
			pageLength: 3
		});
	});
</script>