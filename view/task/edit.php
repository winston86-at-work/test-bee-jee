
<h1 class="page-title">
	<?php echo $page_title; ?>
</h1>
<form action="" method="POST">

	<input type="hidden" name="save_task" value="1">
	<input name="id" type="hidden"value="<?php echo @$data['id']; ?>">
	<input name="is_done" type="hidden"value="<?php echo @$data['is_done']?$data['is_done']:0; ?>">

	<div class="form-group">
		<input required class="form-control" name="name" type="text" placeholder="Name" value="<?php echo @$data['name']; ?>">	
	</div>
	<div class="form-group">
		<input required class="form-control" name="email" type="email" placeholder="Email" value="<?php echo @$data['email']; ?>">
	</div>
	<div class="form-group">
		<input required class="form-control" name="title" type="text" placeholder="Title" value="<?php echo @$data['title']; ?>">
	</div>
	<div class="form-group">
		<textarea required class="form-control" name="content"><?php echo @$data['content']; ?></textarea>
	</div>
	<?php if (!empty($_SESSION['user'])) { ?>
		<div class="form-check">
			<input class="form-check-input" id="is_done" name="is_done" type="checkbox" <?php echo @$data['is_done'] == 1 ? 'checked="checked"':''; ?>  value="1">
			<label for="is_done">Is The Task Is Done</label>
		</div>
	<?php } ?>
	<div class="form-group">
		<button type="submit" class="btn btn-primary">Submit</button>
	</div>
</form>