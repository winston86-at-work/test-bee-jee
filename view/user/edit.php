<h1 class="page-title">
	<?php echo $page_title; ?>
</h1>
<form action="" method="POST">

	<input type="hidden" name="save_user" value="1">
	<input name="id" type="hidden" placeholder="Name" value="<?php echo @$data['id']; ?>">

	<div class="form-group">
		<input class="form-control" name="name" type="text" placeholder="Name" value="<?php echo @$data['name']; ?>">	
	</div>
	<div class="form-group">
		<input class="form-control" name="password" type="password" placeholder="Password" value="<?php echo @$data['password']; ?>">	
	</div>
	<div class="form-group">
		<input class="form-control" name="surname" type="text" placeholder="Surname" value="<?php echo @$data['surname']; ?>">
	</div>
	<div class="form-group">
		<input class="form-control" name="tel" type="text" placeholder="Phone Number" value="<?php echo @$data['tel']; ?>">
	</div>
	<div class="form-group">
		<textarea class="form-control" name="address">
			<?php echo @$data['address']; ?>
		</textarea>
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-primary">Submit</button>
	</div>
</form>