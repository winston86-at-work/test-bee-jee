<h1 class="page-title">
	<?php echo $page_title; ?>
</h1>
<table id="users" class="display">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Surname</th>
            <th>Phone Number</th>
            <th>Address</th>
            <th>Tools</th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach ($data as $key => $user) { ?>
    		<tr>
	            <td><?php echo $user['id'] ?></td>
	            <td><?php echo $user['name'] ?></td>
	            <td><?php echo $user['surname'] ?></td>
	            <td><?php echo $user['tel'] ?></td>
	            <td><?php echo $user['address'] ?></td>
	            <td>
	            	<?php if (!empty($_SESSION['user'])) { ?>
	            		<a class="w-100 btn btn-success" href="/edit?id=<?php echo $user['id']; ?>">
		            		Edit
		            	</a> 
		            	<a class="w-100 btn btn-warning" href="/delete?id=<?php echo $user['id']; ?>">
		            		Delete
		            	</a> 
	            	<?php }else{ ?> 
						!
	            	<?php } ?>
	            </td>
	        </tr>
    	<?php } ?>
    </tbody>
</table>
<?php if (!empty($_SESSION['user'])) { ?>
	<a href="/add" class="btn btn-primary">New User</a>
<?php } ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#users').DataTable();
	});
</script>