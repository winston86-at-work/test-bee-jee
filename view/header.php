<html>
<head>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
</head>
<body>
	<div class="container">
	<?php if (!empty($_SESSION['msg'])) { ?>
		<div class="sys-msg alert alert-success">
			<?php echo $_SESSION['msg']; unset($_SESSION['msg']); ?>
		</div>
	<?php } ?>
	<?php if (!empty($_SESSION['msg-err'])) { ?>
		<div class="sys-msg alert alert-danger">
			<?php echo $_SESSION['msg-err']; unset($_SESSION['msg-err']); ?>
		</div>
	<?php } ?>
	<div class="float-right">
		<?php if(empty($_SESSION['user'])){ ?>
			<a href="/user/login" id="login-link" class="login-link btn btn-primary">
				Login
			</a>
		<?php }else{ ?>
			<a href="/user/logout" id="login-link" class="login-link btn btn-danger">
				Logout
			</a>
		<?php } ?>
	</div>