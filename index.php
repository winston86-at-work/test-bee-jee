<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
ini_set('display_startup_errors',1);

include $_SERVER['DOCUMENT_ROOT'].'/controller/UserController.php';
include $_SERVER['DOCUMENT_ROOT'].'/controller/TaskController.php';
include $_SERVER['DOCUMENT_ROOT'].'/model/Model.php';
include $_SERVER['DOCUMENT_ROOT'].'/model/UserModel.php';
include $_SERVER['DOCUMENT_ROOT'].'/model/TaskModel.php';

$config = array(
	'view_dir' => $_SERVER['DOCUMENT_ROOT'].'/view',
	'db_user' => 'winston86',
	'db_database' => 'winston86',
	'db_password' => 'Id12649839',
	'host' => 'localhost'
);

@list($dump, $controller_name, $action_name) = explode("/", str_replace('?'.$_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']));

if (empty($controller_name)) {
	$controller_name = 'Task';
}

$controller_name .= 'Controller';
$controller_name = "controller\\".$controller_name;

if (empty($action_name)) {
	$action_name = 'view';
}

$action = $action_name.'Action';

$controller = new $controller_name;

ob_start();

include $config['view_dir'].'/header.php';

$controller->{$action}();

include $config['view_dir'].'/footer.php';

$output = ob_get_contents();
ob_end_clean();
echo $output;