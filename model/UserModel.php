<?php

namespace model;

use \model\Model;

class UserModel extends Model{

	public function getUsers(){

		$sql = "SELECT * FROM users";

		$rs = $this->connect->query($sql);

		return $rs->fetch_all(MYSQLI_ASSOC);

	}

	public function getUser($id){

		$sql = "SELECT id, name, surname, tel, address FROM users WHERE id = ?";

		$stmt = $this->connect->prepare($sql);
		$stmt->bind_param('i', $id);
		$stmt->bind_result($id, $name, $surname, $tel, $address);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();

		return array(
			'id' => $id,
			'name' => $name,
			'surname' => $surname,
			'tel' => $tel,
			'address' => $address
		);

	}

	public function getUserByName($name){

		$sql = "SELECT id, name, password, surname, tel, address FROM users WHERE name = ?";

		$stmt = $this->connect->prepare($sql);
		$stmt->bind_param('s', $name);
		$stmt->bind_result($id, $name, $password, $surname, $tel, $address);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();

		return array(
			'id' => $id,
			'name' => $name,
			'password' => $password,
			'surname' => $surname,
			'tel' => $tel,
			'address' => $address
		);

	}

	public function delUser($id){
		$sql = "DELETE FROM users WHERE id = ?";
		$stmt = $this->connect->prepare($sql);
		$stmt->bind_param('i', $id);
		$rs = $stmt->execute();
		$stmt->close();
		return $rs;
	}

	public function getUsersCountByName($name){

		$sql = "SELECT COUNT(id) FROM users WHERE name = ?";

		$stmt = $this->connect->prepare($sql);
		$stmt->bind_param('s', $name);
		$stmt->bind_result($count);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();

		return $count;

	}

	public function saveUser($new = false, $data){


		$is_unique = $this->getUsersCountByName($data['name']) <= 1;

		if ($is_unique && !empty($_SESSION['user'])) {
			if ($new) {
				$sql = "INSERT INTO users (name, password, surname, tel, address) VALUES (?,?,?,?)";
				$stmt = $this->connect->prepare($sql);
				$stmt->bind_param('ssss', 
					htmlentities($data['name']), 
					sha1($data['password']), 
					htmlentities($data['surname']), 
					htmlentities($data['tel']), 
					htmlentities($data['address']));
			}else{
				$sql = "UPDATE users SET name = ?, password = ?, surname = ?, tel = ?, address = ? WHERE id = ?";
				$stmt = $this->connect->prepare($sql);
				$stmt->bind_param('ssssi', 
					htmlentities($data['name']), 
					sha1($data['password']), 
					htmlentities($data['surname']), 
					htmlentities($data['tel']), 
					htmlentities($data['address']), 
					$data['id']);
			}
			$rs = $stmt->execute();
			$stmt->close();
		}else{
			$rs = false;
		}


		return $rs;

	}


}