<?php

namespace model;

class Model {
	
	protected $connect;

	function __construct(){

		global $config;

		$this->connect = new \mysqli($config['host'], $config['db_user'], $config['db_password'], $config['db_database']);
		$this->connect->set_charset("utf8");
	}


}