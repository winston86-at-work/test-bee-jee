<?php

namespace model;

use \model\Model;

class TaskModel extends Model{

	public function getTasks(){

		$sql = "SELECT * FROM tasks";

		$rs = $this->connect->query($sql);

		return $rs->fetch_all(MYSQLI_ASSOC);

	}

	public function getTask($id){

		$sql = "SELECT id, name, email, title, content, is_done, created FROM tasks WHERE id = ?";

		$stmt = $this->connect->prepare($sql);
		$stmt->bind_param('i', $id);
		$stmt->bind_result($id, $name, $email, $title, $content, $is_done, $created);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();

		return array(
			'id' => $id,
			'name' => $name,
			'email' => $email,
			'title' => $title,
			'content' => $content,
			'is_done' => $is_done,
			'created' => $created
		);

	}

	public function delTask($id){
		$sql = "DELETE FROM tasks WHERE id = ?";
		$stmt = $this->connect->prepare($sql);
		$stmt->bind_param('i', $id);
		$rs = $stmt->execute();
		$stmt->close();
		return $rs;
	}

	public function saveTask($new = false, $data){


		if ($new) {
			$sql = "INSERT INTO tasks (name, email, title, content, is_done, created) VALUES (?,?,?,?,?,?)";
			$stmt = $this->connect->prepare($sql);
			$is_done = 0;
			$date = date("Y-m-d H:i:s");
			$stmt->bind_param('ssssis', 
				htmlentities($_POST['name']), 
				htmlentities($_POST['email']), 
				htmlentities($_POST['title']), 
				htmlentities($_POST['content']), 
				$is_done, 
				$date);
		}else{
			$sql = "UPDATE tasks SET name = ?, email = ?, title = ?, content = ?, last_editor = ?, is_done = ? WHERE id = ?";
			$stmt = $this->connect->prepare($sql);
			$stmt->bind_param('sssssii', 
				htmlentities($_POST['name']), 
				htmlentities($_POST['email']), 
				htmlentities($_POST['title']), 
				htmlentities($_POST['content']),
				htmlentities($_SESSION['user']['name']),
				$_POST['is_done'], 
				$_POST['id']);
		}
		$rs = $stmt->execute();
		$stmt->close();

		return $rs;

	}


}