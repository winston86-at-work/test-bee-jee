<?php

namespace controller;



use \model\TaskModel;


class TaskController {



	public function indexAction(){
		$this->viewAction();
	}


	public function viewAction(){

		$taskModel = new TaskModel();

		$tasks = $taskModel->getTasks();

		$this->renderView('view', $tasks, 'Task List');


	}

	public function editAction(){

		if (empty($_SESSION['user'])) {
			$_SESSION['msg-err'] = "Вы не имеете права на редактирование задач!";
			$this->redirect('/user/login');
		}

		$taskModel = new TaskModel();

		$task = $taskModel->getTask($_GET['id']);

		$this->renderView('edit', $task, 'Edit Task');

		if (isset($_POST['save_task'])) {
			$rs = $taskModel->saveTask(false, $_POST);
			if ($rs) {
				$_SESSION['msg'] = "Задание сохранено!";
				$this->redirect('/');
			}
		
		}

	}

	public function deleteAction(){

		$taskModel = new TaskModel();

		if($taskModel->delTask($_GET['id'])){
			$_SESSION['msg'] = "Задание удалено!";
			$this->redirect('/');
		}

	}

	public function addAction(){

		$taskModel = new TaskModel();

		$this->renderView('edit', array(), 'Add Task');

		if (isset($_POST['save_task'])) {
			
			$rs = $taskModel->saveTask(true, $_POST);

			if ($rs) {
				$_SESSION['msg'] = "Задание добавлено!";
				$this->redirect('/');
			}

		}

	}

	private function renderView($view, $data = array(), $page_title = ''){

		global $config;

		include $config['view_dir'].'/task/'.$view.'.php';

	}

	private function redirect($url){
		session_write_close();
		header('Location: '.$url);
		exit();
	}


}