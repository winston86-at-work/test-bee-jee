<?php

namespace controller;



use \model\UserModel;


class UserController {



	public function indexAction(){
		$this->viewAction();
	}


	public function viewAction(){

		$userModel = new UserModel();

		$users = $userModel->getUsers();

		$this->renderView('view', $users, 'User List');


	}

	public function editAction(){

		$userModel = new UserModel();

		$user = $userModel->getUser($_GET['id']);

		$this->renderView('edit', $user, 'Edit User');

		if (isset($_POST['save_user'])) {

			$rs = $userModel->saveUser(false, $_POST);

			if ($rs) {
				$this->redirect('/');
			}
		
		}

	}

	public function deleteAction(){

		$userModel = new UserModel();

		if($userModel->delUser($_GET['id'])){
			$this->redirect('/');
		}

	}

	public function addAction(){

		$userModel = new UserModel();

		$this->renderView('edit', array(), 'Add User');

		if (isset($_POST['save_user'])) {
			
			$rs = $userModel->saveUser(true, $_POST);

			if ($rs) {
				$this->redirect('/');
			}

		}

	}

	public function loginAction(){

		$userModel = new UserModel();

		$this->renderView('login', array(), 'Login');

		if (isset($_POST['login_user'])) {

			$user = $userModel->getUserByName($_POST['name']);
			
			$rs = !empty($user) && $user['password'] == sha1($_POST['password']);

			if ($rs) {
				$_SESSION['user'] = $user;
				$this->redirect('/');
			}else{
				$_SESSION['msg-err'] = 'Логин или пароль не верны!';
			}

		}

	}

	public function logoutAction(){

		unset($_SESSION['user']);

		$this->redirect('/');

	}

	private function renderView($view, $data = array(), $page_title){

		global $config;

		include $config['view_dir'].'/user/'.$view.'.php';

	}

	private function redirect($url){
		session_write_close();
		header('Location: '.$url);
		exit();
	}


}